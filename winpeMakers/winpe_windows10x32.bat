@echo off
setlocal EnableDelayedExpansion

set var_rep_dest=d:\winpe_x86
set var_rep_dest_win=%var_rep_dest%\winpe-build
set var_windows_adk_dir=C:\Program Files (x86)\Windows Kits
set var_emplacement_reseau_windows10x32=192.168.1.69
set var_repertoire_reseau_windows10x32=\\%var_emplacement_reseau_windows10x32%\tftp\images\windows\windows10prox32
set var_iso_image_name=windows10prox86.iso

echo|set /p="- Creation de l'archive                                               "
call copype x86 %var_rep_dest_win% > nul
echo OK

echo|set /p="- Montage de l'image pour edition                                     "
Dism /Mount-Image /ImageFile:"%var_rep_dest_win%\media\sources\boot.wim" /index:1 /MountDir:"%var_rep_dest_win%\mount" > nul
echo OK

echo|set /p="- Chargement des packs francais (1/2)                                 "
Dism /image:%var_rep_dest_win%\mount /Add-Package /packagePath:"%var_windows_adk_dir%\10\Assessment and Deployment Kit\Windows Preinstallation Environment\x86\WinPE_OCs\WinPE-Scripting.cab" > nul
echo OK
echo|set /p="- Chargement des packs francais (2/2)                                 "
Dism /image:%var_rep_dest_win%\mount /Add-Package /packagePath:"%var_windows_adk_dir%\10\Assessment and Deployment Kit\Windows Preinstallation Environment\x86\WinPE_OCs\fr-fr\lp.cab" > nul
echo OK

echo|set /p="- Mise en francais de l'interface                                     "
dism /image:%var_rep_dest_win%\mount /Set-UILang:fr-FR > nul
echo OK
echo|set /p="- Mise en francais du systeme                                         "
dism /image:%var_rep_dest_win%\mount /Set-SysLocale:fr-FR > nul
echo OK
echo|set /p="- Mise en francais des parametres utilisateur                         "
dism /image:%var_rep_dest_win%\mount /Set-UserLocale:fr-FR > nul
echo OK
echo|set /p="- Mise en francais des parametres locaux                              "
dism /image:%var_rep_dest_win%\mount /Set-InputLocale:fr-FR > nul
echo OK

echo|set /p="- Creation du fichier d'initialisation windows                        "
set var_system_drive=SystemDrive
set var_startnet_path=%var_rep_dest_win%\mount\windows\system32\startnet.cmd 

rem Remplissage du fichier
set var_fichier_init1=@echo off
set var_fichier_init3=wpeinit
set var_fichier_init4=echo Connexion au repertoire reseau...
set "var_fichier_init5=ping %var_emplacement_reseau_windows10x32% -n 21 -w 1000 > nul"
set "var_fichier_init6=net use z: %var_repertoire_reseau_windows10x32% /user:guest "">nul"
set var_fichier_init7=echo Lancement de l'installation...
set var_fichier_init8=z:\setup.exe
echo %var_fichier_init1% > %var_startnet_path%
echo %var_fichier_init3% >> %var_startnet_path%
echo %var_fichier_init4% >> %var_startnet_path%
echo !var_fichier_init5! >> %var_startnet_path%
echo !var_fichier_init6! >> %var_startnet_path%
echo %var_fichier_init7% >> %var_startnet_path%
echo %var_fichier_init8% >> %var_startnet_path%
echo OK

echo|set /p="- Copie du bootmanager                                                "
copy %var_rep_dest_win%\mount\Windows\Boot\PXE\bootmgr.exe %var_rep_dest_win%\ > nul
echo OK
echo|set /p="- Suppression du fichier "bootfix.bin"                                "
del %var_rep_dest_win%\media\Boot\bootfix.bin
echo OK

echo|set /p="- Validation des changements                                          "
dism /unmount-wim /mountdir:%var_rep_dest_win%\mount /commit > nul
echo OK

echo - Creation du fichier ISO
call MakeWinPEMedia /ISO %var_rep_dest_win% %var_rep_dest%\%var_iso_image_name% > nul
cd \ > nul
rmdir /Q /S %var_rep_dest_win% > nul
cd %var_rep_dest% > nul
echo L'image "%var_iso_image_name%" a ete cree dans le repertoire "%var_rep_dest%"
pause