# Petit PXE maison

![Logo projet](https://gitlab.com/lordashram/pxe/-/raw/master/media_files/readme/icon.png "Logo du projet")

## Objectif

Avoir un PXE sur mon NAS afin de pouvoir remettre facilement un OS sur une machine ou créer rapidement une VM sur l'OS voulu.

Le \"README.md\" présente ici le simple fonctionnement du projet dans son ensemble. Pour une présentation complète et détaillée, se référer au [Wiki du projet](https://gitlab.com/lordashram/pxe/-/wikis/home) qui fera office de tutoriel.

## Fonctionnement

### Prérequis

* Avoir configuré son PXE.
* Mis dans le dossier "pxelinux.cfg" le contenu du projet.
* Créé un dossier "images" sur le PXE et mis les images que vous voulez installer (se référer aux tutos ou au Wiki).

### Lancement

Configurer un ordinateur ou une VM pour qu'il démarre sur le LAN. Démarrer le PC ou la VM et hop ^^.

### L'accueil

L'accueil permet différentes actions :
* Démarrer sur le disque dur de la machine
* Voir les différentes installations Windows
* Voir les différentes installations Linux
* Lancer un test de mémoire
* Redémarrer la machine
* Arrêter la machine

![Menu d'accueil](https://gitlab.com/lordashram/pxe/-/raw/master/media_files/readme/accueil.png "Menu d'accueil")

### Windows

Les installations de Windows sont pour l'instant :
* Windows 7 x86
* Windows 7 x64
* Windows 10 x86
* Windows 10 x64
* Windows 2012 Server R2 Essentials x64

![Menu Windows](https://gitlab.com/lordashram/pxe/-/raw/master/media_files/readme/windows.png "Menu Windows")

### Linux

Les installations Linux sont plétoriques, j'ai choisi uniquement 4 distributions et uniquement en mode x64, mais libre à chacun de faire son adaptation.
J'ai pour l'instant choisi les distributions les plus utilisées :
* Ubuntu
* Xubuntu (parce que je l'aime bien ^^)
* Debian
* Mint

![Menu Linux](https://gitlab.com/lordashram/pxe/-/raw/master/media_files/readme/linux.png "Menu Linux")

### Choix d'un OS

Il suffit de sélectionner un OS et si vous avez tout correctement configuré côté TFTP et que le fichier "default" est en concordance (niveau fichiers/dossiers) tout devrait bien se passer et le démarrage va s'effectuer (ça peut prendre un peu de temps 2/3 minutes suivant la connexion réseau disponible).

## Les fichiers ".bat" dans le dossier "winpeMakers"

J'ai plutôt galéré à faire mes images WinPE pour booter Windows.
Comme j'ai du faire de très nombreux essais afin de créer l'image adéquat et que j'en avais mare de faire des copier/coller à chaque fois depuis les fichiers d'informations que je m'étais fait au travers des différentes recherches. J'ai choisi de scripter la création de mes images WinPE, c'était beaucoup plus simple. De plus, cela permettra à un utilisateur plus ou moins avancé de modifier les fichiers pour générer ce qui lui convient.

Comme il y a 5 images Windows différentes, il y a donc 5 images WinPE à faire pour correspondre à l'installation de chacun.

### Utilisation

#### Prérequis

Avoir installé le créateur d'image correspondant à la version de Windows souhaitée :
* Windows 7 => Windows AIK : [microsoft.com >> Centre de téléchargement >> Kit d'installation automatisée (AIK) Windows pour Windows 7](https://www.microsoft.com/fr-fr/download/details.aspx?id=5753 "Kit d'installation automatisée (AIK) Windows pour Windows 7")
* Windows 10 => Windows ADK : [microsoft.com >> Centre de téléchargement >> Kit de déploiement et d'évaluation Windows (ADK) pour Windows 8](https://www.microsoft.com/fr-FR/download/details.aspx?id=30652 "Kit de déploiement et d'évaluation Windows (ADK) pour Windows 8") (oui, c'est marqué Windows 8, mais c'est celui du 10 aussi...)
* Être administrateur de l'ordinateur utilisé.
* Être sur l'OS voulu. Je n'ai pas réussi à créer d'image Windows 7 sur Windows 10, donc je me suis rapidement monté une petite VM (2 coeurs, 4Go de ram et 50Go de disque sur une [VirtualBox](https://www.virtualbox.org/ "virtualbox.org")) avec un Windows 7, c'était mille fois plus simple.

#### Fonctionnement

Prendre le fichier ".bat" qui correspond à l'image qui est souhaitée. Le mettre où vous le souhaitez.
* Pour Windows 7 : lancer **en mode administrateur** (clic droit => "Exécuter en tant qu'administrateur") "Invite de commande des outils de déploiement"  
![Windows 7 >> Invite de commande des outils de déploiement](https://gitlab.com/lordashram/pxe/-/raw/master/media_files/readme/aik.png "Windows 7 >> Invite de commande des outils de déploiement")

* Pour Windows 10 : lancer **en mode administrateur** (clic droit => "Exécuter en tant qu'administrateur") "Environnement de déploiement et d'outils de création d'images"  
![Windows 10 >>Environnement de déploiement et d'outils de création d'images](https://gitlab.com/lordashram/pxe/-/raw/master/media_files/readme/edk.png "Windows 10 >>Environnement de déploiement et d'outils de création d'images")

* Se déplacer là où vous avez mis le fichier ".bat" et lancer la commande du fichier (j'avais mis mon ".bat" à la racine de mon disque secondaire, c'était beaucoup plus simple...).  
La création de l'image prend entre 30 secondes et 2 minutes suivant la machine et l'OS voulu.

Si toutes les étapes ont été suivies, l'image se crée à l'endroit que vous aurez configuré dans le ".bat" sinon j'ai mis un répertoire par défaut et il n'y aura plus qu'à la déposer dans le dossier d'images du PXE.

De base l'image initiée est vraiment la plus simple possible, j'ai simplement ajouté la prise en charge du français et le lancement automatique de l'installation.

Libre à chacun d'y ajouter ses drivers, ses applications ou plein de configurations spécifiques...bon courage...

Exemple avec la création d'une image Windows 10 x64  
![Creation d'image WinPE](https://gitlab.com/lordashram/pxe/-/raw/master/media_files/readme/winpe_done.png "Creation d'image WinPE Windows 10 x64")


## Liens utiles

J'ai suivi de nombreux tutoriels ou sites de documentation, voici ma liste de favoris pour ce projet :
* [Nas-Forum.com >> Bien démarrer avec votre Synology > Tutoriels > Créer un serveur PXE et installer un Windows 7 ou 10](https://www.nas-forum.com/forum/topic/53386-cr%C3%A9er-un-serveur-pxe-et-installer-un-windows-7-ou-10/)
* [NextInpact >> News >> Installons Linux et Windows depuis le réseau (PXE) simplement, de manière automatisée](https://www.nextinpact.com/news/107512-installons-linux-et-windows-depuis-reseau-pxe-simplement-maniere-automatisee.htm)
* [La page personnelle de Geoffray >> Un PXE pour les unir, tous…](https://www.geoffray-levasseur.org/tutoriels-2/un-pxe-pour-les-unir-tous/)
* [Informatiweb >> InformatiWeb >> Tutoriels >> Informatique >> Windows >> Windows PE - Créer une image Windows PE 3.0 (ou 3.1)](https://www.informatiweb.net/tutoriels/informatique/7-windows/71--windows-pe-creer-une-image-windows-pe-3-0-ou-3-1.html)
* [Paul's Internet Landfill >> 2013 >> Scripted Installations of Windows 7 via PXE](http://pnijjar.freeshell.org/2013/win7-pxe/)
* [Tecmint >> Installing Windows 7 over PXE Network Boot Server on RHEL/CentOS 7 using WinPE ISO Image – Part 2](https://www.tecmint.com/installing-windows-7-over-pxe-network-boot-in-centos/)
* [Vercot >> Serva >> Serva PXE/BINL - AN04: Custom menu](https://www.vercot.com/~serva/an/MenuPXE4.html)
* [HowToForge >> Install Debian 9 (Stretch) via PXE Network Boot Server](https://www.howtoforge.com/tutorial/install-debian-9-stretch-via-pxe-network-boot-server/)
* [Doc.ycharbi.fr >> Wiki doc >> Windows Preinstallation Environment](https://doc.ycharbi.fr/index.php/Windows_Preinstallation_Environment)
* [Syslinux >> Wiki >> Comboot/menu.c32](https://wiki.syslinux.org/wiki/index.php?title=Comboot/menu.c32)
* [IT-Connect >> Cours - Tutoriels >> Administration Systèmes >> Windows Server >> Déploiement MDT - WDS >> Présentation de WinPE](https://www.it-connect.fr/presentation-de-winpe/)
* [Geek-Buyer >> Articles >> Le coin du technicien >> Installation >> TUTO - Installation de MDT 8450](https://www.geek-buyer.fr/2019/01/tuto-dinstallation-mdt-8450/)